# Machine Learning

Machine Learning


1.  Minggu 1 - RPS & Instalasi Anaconda 
2.  [Minggu 2 - Pengenalan Prosedur dalam Machine Learning menggunakan Scikit Learn](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m2)
3.  [Minggu 3 - Pengenalan Unsupervised Learning dan Implementasi Algoritma K-Means](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m3)
4.  [Minggu 4 - Hierarchical Clustering](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m4)
5.  [Minggu 5 - Naive Bayes](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m5)
6.  [Minggu 6 - Data Preprocessing](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m6)
7.  [Minggu 7 - Decision Tree](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m7)
8. [Minggu 8 - UTS - Prediksi Hasil Rekomendasi Berdasarkan Review Game pada Platform Steam menggunakan K-Nearest Neighbors](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/UTS)
9.  [Minggu 9 - K-Nearest Neighbor](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m9)
10.  [Minggu 10 - Linear Regresi](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m10)
11.  [Minggu 11 - Ensemble Learning](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m11)
12.  [Minggu 12 - Neural Network Perceptron](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m12)
13.  [Minggu 13 - Neural Network Perceptron](https://gitlab.com/sabrisangjaya/machine-learning/tree/master/m12)